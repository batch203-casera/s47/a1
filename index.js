let color = document.querySelector('.text-color');
let firstname = document.getElementById('firstname');
let lastname = document.getElementById('lastname');

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

const fullName = () => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

color.addEventListener('click', el => {
    spanFullName.style.color = el.target.value;
    color.addEventListener("change", e => {
        spanFullName.style.color = e.target.value;
    });
})